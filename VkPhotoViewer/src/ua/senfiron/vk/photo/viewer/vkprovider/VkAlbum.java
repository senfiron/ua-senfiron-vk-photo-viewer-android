package ua.senfiron.vk.photo.viewer.vkprovider;

import java.util.ArrayList;


public class VkAlbum {
    int mAlbumId;
    String mTitle;
    VkPhoto mThumb;
    private ArrayList<VkPhoto> mPhotos = new ArrayList<VkPhoto>();
    
    public VkAlbum(int albumId, String title, VkPhoto thumb)
    {
        this.mAlbumId = albumId;
        this.mTitle = title;
        this.mThumb = thumb;
    }
    
    public VkPhoto getThumb() {
        return mThumb;
    }

    public int getAlbumID() {
        return mAlbumId;
    }

    public String getTitle() {
        return mTitle;
    }
    
    public ArrayList<VkPhoto> getPhotos()
    {
        return mPhotos;
    }

    public void setPhotos(ArrayList<VkPhoto> photos) {
        this.mPhotos = photos;
    }
}
