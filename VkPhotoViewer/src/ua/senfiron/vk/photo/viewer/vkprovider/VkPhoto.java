package ua.senfiron.vk.photo.viewer.vkprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class VkPhoto {

    public static class VkPhotoInfo
    {
        public String getSrc() {
            return mSrc;
        }

        public int getWidth() {
            return mWidth;
        }

        public int getHeight() {
            return mHeight;
        }

        public String getType() {
            return mType;
        }

        private String mSrc;
        private int mWidth;
        private int mHeight;
        private String mType;
        
        public VkPhotoInfo(String src, int width, int height, String type)
        {
            this.mSrc = src;
            this.mHeight = height;
            this.mWidth = width;
            this.mType = type;
        }
    }
    private class VKPhotoInfoComparator implements Comparator<VkPhotoInfo> {
        @Override
        public int compare(VkPhotoInfo lhs, VkPhotoInfo rhs) {

            if(lhs.getWidth() > rhs.getWidth()) return +1;
            if(lhs.getWidth() < rhs.getWidth()) return -1;
            return 0;
        }
    }
    private int mPhotoId;
    private int mAlbumId;
    private String mText;
    private ArrayList<VkPhotoInfo> mSizesInfo;
        
    public VkPhoto(int photoID, int albumID, String text)
    {
        this.mPhotoId = photoID;
        this.mAlbumId = albumID;
        this.mText = text;
    }
    
    public int getPhotoID() {
        return mPhotoId;
    }

    public int getAlbumID() {
        return mAlbumId;
    }

    public String getText() {
        return mText;
    }
    
    public ArrayList<VkPhotoInfo> getSizesInfo() {
        return mSizesInfo;
    }

    public void setSizesInfo(ArrayList<VkPhotoInfo> sizesInfo) {
        mSizesInfo = sizesInfo;
        Collections.sort(mSizesInfo, new VKPhotoInfoComparator());
    }
    
    public String getOptimalThumbSrc(int preferedEdgeSize)
    {
        if(mSizesInfo == null) return null;

        for (VkPhotoInfo thumb : mSizesInfo) {
            if(Math.min(thumb.getHeight(), thumb.getWidth()) >= preferedEdgeSize)
                return thumb.getSrc();
        }
        return mSizesInfo.get(mSizesInfo.size()-1).getSrc();
    }
}
