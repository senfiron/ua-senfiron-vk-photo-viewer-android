package ua.senfiron.vk.photo.viewer;

import ua.senfiron.vk.photo.viewer.vkprovider.VkPhoto;
import ua.senfiron.vk.photo.viewer.vkprovider.VkProvider;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

public class ViewPhotoFragment extends Fragment {

    static final String ARGUMENT_PHOTO_INDEX = "arg_photo_index";
    private int mPhotoIndex;
    private VkProvider mVkProvider;

    static ViewPhotoFragment newInstance(int position) {
        ViewPhotoFragment pageFragment = new ViewPhotoFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PHOTO_INDEX, position);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPhotoIndex = getArguments().getInt(ARGUMENT_PHOTO_INDEX);
        mVkProvider = VkProvider.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_photo, null);

        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        
        VkPhoto photo = mVkProvider.getCurrentAlbum().getPhotos().get(mPhotoIndex);
        
        String url = photo.getOptimalThumbSrc(width);
        
        AQuery aq = new AQuery(view);
        aq
        .id(R.id.imageViewViewPhoto)
        .progress(R.id.progressBarViewPhoto)
        .image(url,false,true,width,R.drawable.empty_photo,null,AQuery.FADE_IN_NETWORK);

        return view;
    }
}
