package ua.senfiron.vk.photo.viewer;

import java.util.ArrayList;

import ua.senfiron.vk.photo.viewer.ConnectivityChangeReceiver.ConnectivityChangeListener;
import ua.senfiron.vk.photo.viewer.util.GridViewUtilities;
import ua.senfiron.vk.photo.viewer.util.NetworkUtilities;
import ua.senfiron.vk.photo.viewer.vkprovider.VkAlbum;
import ua.senfiron.vk.photo.viewer.vkprovider.VkProvider;
import ua.senfiron.vk.photo.viewer.vkprovider.VkProvider.OnAlbumsLoadedListener;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ProgressBar;

public class AlbumListActivity extends ActionBarActivity {

    private int mImageThumbSize;
    private int mImageThumbSpacing;
    private ProgressBar mProgressBar;
    private GridView mGridView;
    private VkProvider mVkProvider;
    private VkPhotoAdapterAbstract<VkAlbum> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_grid);
        // Show the Up button in the action bar.
        setupActionBar();


        mVkProvider = VkProvider.getInstance();
        mProgressBar = (ProgressBar) findViewById(R.id.progressBarImageGrid);
        mGridView = (GridView) findViewById(R.id.gridViewImageGrid);

        if(mVkProvider.getAlbums().isEmpty()) {
            refresh();
        } else {
            mProgressBar.setVisibility(View.GONE);
            mGridView.setVisibility(View.VISIBLE);
        }
        
        mAdapter = new VkPhotoAdapterAbstract<VkAlbum>(mVkProvider.getAlbums()) {

            @Override
            public String getItemText(int position) {
                return mArray.get(position).getTitle();
            }

            @Override
            public String getItemImageUrl(int position, int itemHeight) {
                return mArray.get(position).getThumb().getOptimalThumbSrc(itemHeight);
            }
        };

        mGridView.setAdapter(mAdapter);

        mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);

        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        GridViewUtilities.gridViewUpdateCellSize(mGridView, mAdapter, mImageThumbSize, mImageThumbSpacing);
                    }
                });

        mGridView.setOnItemClickListener(
                new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                        mVkProvider.setCurrentAlbumIndex((int) id);
                        goForward();
                    }
                });
    }
    
    private void refresh() {

        if(!ConnectivityChangeReceiver.isNetworkConnected(this)) {
            NetworkUtilities.showInternetNotAvaliableDialog(this);
            ConnectivityChangeReceiver.setListener(new ConnectivityChangeListener() {
                
                @Override
                public void onNetworkConnected() {
                    refresh();
                }
            });
        }
        mGridView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);

        mVkProvider.loadAlbumsAsync(new OnAlbumsLoadedListener() {

            @Override
            public void onAlbumsLoaded(ArrayList<VkAlbum> albums) {

                if(mAdapter != null)
                {
                    mAdapter.notifyDataSetChanged();
                }
                mProgressBar.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);

            }
        });
    }
    
    private void goForward() {
        if(mVkProvider.getCurrentAlbum() != null) {
            Intent intent = new Intent(AlbumListActivity.this, PhotoListActivity.class);
            startActivity(intent);
        }           
    }
    
    private void logout() {
        mVkProvider.logOut(this);
        Intent intent = new Intent(AlbumListActivity.this, VkLoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // Removes other Activities from stack
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
     }


    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            logout();
            return true;
            
        case R.id.action_settings:
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
 
        case R.id.action_refresh:
            refresh();
            return true;
            
        case R.id.action_forward:
            goForward();
            return true;
            
        case R.id.action_back:
        case R.id.action_logout:
            logout();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
