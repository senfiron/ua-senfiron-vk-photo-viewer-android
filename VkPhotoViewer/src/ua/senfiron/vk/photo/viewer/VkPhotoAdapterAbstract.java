package ua.senfiron.vk.photo.viewer;

import java.util.ArrayList;
import java.util.List;

import ua.senfiron.vk.photo.viewer.ConnectivityChangeReceiver.ConnectivityChangeListener;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.androidquery.AQuery;

public abstract class VkPhotoAdapterAbstract<T> extends BaseAdapter  {

    protected List<T> mArray;

    public abstract String getItemText(int position);
    public abstract String getItemImageUrl(int position, int itemHeight);

    private int mNumColumns = 0;
    private int mItemHeight = 0;
    private GridView.LayoutParams mImageViewLayoutParams;

    public VkPhotoAdapterAbstract(ArrayList<T> array) {
        super();
        mArray = array;

        mImageViewLayoutParams = new GridView.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }


    public int getNumColumns() {
        return mNumColumns;
    }

    public void setNumColumns(int numColumns) {
        mNumColumns = numColumns;
    }

    public void setItemHeight(int itemHeight) {
        if (itemHeight == mItemHeight) {
            return;
        }
        mItemHeight = itemHeight;
        mImageViewLayoutParams =
                new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mItemHeight);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mArray.size();
    }

    @Override
    public Object getItem(int position) {
        return mArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View itemView = null;
        final Context context = parent.getContext();
        if (convertView == null) { // if it's not recycled, instantiate and initialize
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.image_grid_item, null);
            itemView.setLayoutParams(mImageViewLayoutParams);
        } else { // Otherwise re-use the converted view
            itemView = (View) convertView;
        }

        if (itemView.getLayoutParams().height != mItemHeight) {
            itemView.setLayoutParams(mImageViewLayoutParams);
        }

        int edgeSize = itemView.getLayoutParams().height;
        
        if(edgeSize <= 0)
            return itemView;
        
        String title = getItemText(position);
        String url = getItemImageUrl(position,edgeSize);

        AQuery aq = new AQuery(itemView);

        if(title != null && title.length() != 0) {
            aq.id(R.id.textViewImageGridItem).text(title).visible();
        } else {
            aq.id(R.id.textViewImageGridItem).gone();
        }

        aq
        .id(R.id.imageViewImageGridItem)
        .progress(R.id.progressBarImageGridItem)
        .image(url,true,true,edgeSize,R.drawable.empty_photo);
        
        if(!ConnectivityChangeReceiver.isNetworkConnected(context)) {
            ConnectivityChangeReceiver.setListener(new ConnectivityChangeListener() {
                
                @Override
                public void onNetworkConnected() {
                    notifyDataSetChanged();
                }
            });
        }

        return itemView;
    }
}
