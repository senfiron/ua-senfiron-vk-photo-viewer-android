package ua.senfiron.vk.photo.viewer.util;

import ua.senfiron.vk.photo.viewer.R;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtilities {
    
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = connManager.getActiveNetworkInfo();
        if (i != null && i.isConnected())
            return true;
        return false;
    }
    
    public static void showInternetNotAvaliableDialog(Context context)
    {
        Builder alert = new AlertDialog.Builder(context);
        alert.setMessage(R.string.internet_not_avaliable);
        alert.setPositiveButton("OK", null);
        alert.create();
        alert.show();
    }

}
