package ua.senfiron.vk.photo.viewer.util;

public class StringUtilities {
    public static final String stringBetween(String source, String str1, String str2)
    {
        if(!source.contains(str1))
            return null;
        
        int start = source.indexOf(str1) + str1.length();
        int end = source.indexOf(str2, start);
        if(end == -1) end = source.length();
        String result = source.substring(start, end);
        
        return result;

    }
}
