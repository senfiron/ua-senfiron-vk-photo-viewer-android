package ua.senfiron.vk.photo.viewer.util;

import ua.senfiron.vk.photo.viewer.VkPhotoAdapterAbstract;
import android.widget.GridView;

public class GridViewUtilities {

    public static void gridViewUpdateCellSize(GridView gridview, VkPhotoAdapterAbstract<?> adapter, int cellSize, int cellSpacing)
    {
        if (adapter.getNumColumns() == 0) {
            
            final int numColumns = (int) Math.floor(
                    gridview.getWidth() / (cellSize + cellSpacing));
            if (numColumns > 0) {
                final int columnWidth =
                        (gridview.getWidth() / numColumns) - cellSpacing;
                adapter.setNumColumns(numColumns);
                adapter.setItemHeight(columnWidth);
                gridview.setNumColumns(numColumns);
                gridview.setColumnWidth(columnWidth);
            }
        }
    }
}
